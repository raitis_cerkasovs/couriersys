<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h2>Authorized users only</h2>

<!-- admin function to print all users, not used in project -->
<c:forEach var="user" items="${users}">
  <p>
  
  <c:out value="${user.username}"></c:out>
  
  || 
  <c:out value="${user.password}"></c:out>
  
  ||
  <c:out value="${user.email}"></c:out>
<%--     
  ||
  <c:out value="${user.iscourier}"></c:out> --%>

  || 
  <c:out value="${user.enabled}"></c:out>
 
  ||
  <c:out value="${user.created}"></c:out>
  
  || 
  <c:out value="${user.authorities.authority}"></c:out>
  </p>
</c:forEach>

