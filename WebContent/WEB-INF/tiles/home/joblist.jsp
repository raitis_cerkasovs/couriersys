<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="help">
<p>Only reports of the current and committed jobs.</p>
<p>The main focus in application is on the client's side. </p>
</div>


<!-- courier web console. Print all his jobs from "jobs" list  -->

<c:forEach var="job" items="${jobs}">

<table style="border: 1px solid brown; margin:10px;">


<tr>
<td>Courier:</td><td colspan="3" style="color: grey; text-align: left" >${job.courier.user.name}&nbsp;${job.courier.user.surname},&nbsp;tel:&nbsp;${job.courier.user.phone},&nbsp;&nbsp;email:&nbsp;${job.courier.user.email}&nbsp;</td>
</tr>

<tr>
<td>From:</td><td colspan="3"  style="text-align: left;color:gray;">${job.sendfrom.name},&nbsp;${job.sendfrom.address1}&nbsp;${job.sendfrom.address2},&nbsp;${job.sendfrom.postcode}</td>
</tr>

<tr>
<td>To:</td> <td colspan="3"  style="text-align: left;color:gray;">${job.sendto.name},&nbsp;${job.sendto.address1}&nbsp;${job.sendto.address2},&nbsp;${job.sendto.postcode}</td>
</tr>

<tr>
<td style="border-bottom: 1px solid brown;">Notes:</td> <td colspan="3"  style="text-align: left;color:gray; border-bottom: 1px solid brown;">${job.notes}</td>
</tr>

<tr>
 <td>Status</td><td>Created</td><td>Price</td><td>Distance</td>
</tr>

<tr>
 <td style="color: grey;">${job.status}</td><td style="font-size:small; color: grey;">${job.created}</td><td style="color: grey;">${job.price}</td><td style="color: grey;">${job.distance}</td>
</tr>

</table>
</c:forEach>

<div class="mainMenu"><a style="text-align: left;" href="<c:url value="/" />">Home</a></div>
