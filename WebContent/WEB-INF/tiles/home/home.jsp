<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="help">
<p>You have to be registered to use an application.</p> 
<p>There are two types of registration - one for couriers and one for clients. Both registrations are 4 step process to fill the related information.</p>
<p>To use an existing registration to log-in as a courier:<br>username: <b>testcourier1</b><br>password: <b>testcourier1</b><br></p>
<p>To log in as a client:<br>username: <b>testclient1</b><br>password: <b>testclient1</b><br></p>
<p>Courier's mobile application can be downloaded from the related link.</p>
<p>After authorisation "Bookings" link appears for clients and "My Jobs" for couriers</p>
<p>After authorisation "Edit Account" link appears at the top of the page to edit user's details.</p>
<p>Menu "Information" holds only general information about the project.</p>
</div>


<p>
	MSc ACT Course Project. Couriers Online System. Proposed to organize
	couriers online job flow,<br /> including registration, tracing status
	and proof of delivery.
</p>

<table class="mainMenu">
	<tr>

		<td class="mainMenu">
		  <p>
		    <a class="mainMenu" href="<c:url value="/customer-registration" />"> Registration for Clients</a> 
		  </p> 
		 
		  <p>
		    <br /> Download Client's App: 
		  </p> 
		 
		  <p>
		    <img width="100" height="30" src="${pageContext.request.contextPath}/static/img/gp3.jpeg" /> <br><span style="font-size: 10px; color:red;">not included in scope of this project</span><br><br>
		    <img width="100" height="30" src="${pageContext.request.contextPath}/static/img/iph.jpeg" /> <br><span style="font-size: 10px; color:red;">not included in scope of this project</span>
	      </p>
	   </td>	
			
			
	   <td class="mainMenu">
		  <p>
		    <a class="mainMenu" href="<c:url value="/courier-registration" />">Registration for Couriers</a> 
		  </p> 
		 
		  <p>
		    <br /> Download Courier's App: 
		  </p> 
		  <p> 
		    <a href="${pageContext.request.contextPath}/static/apk/CourierAndroidApp.apk">
              <span style="color:green;">download link for android app</span>
            </a> 
		    
		    <br><br><br>
		    
		    <img width="100" height="30" src="${pageContext.request.contextPath}/static/img/iph.jpeg" /><br><span style="font-size: 10px; color:red;">not included in scope of this project</span>
		    
	    </td>

	</tr>
</table>

<sec:authorize access="isAuthenticated()">
  
  <sec:authorize access="hasRole('ROLE_CUSTOMER')">
     <div class="mainMenu">
	    <a href="${pageContext.request.contextPath}/availability">Bookings</a>
     </div>  
  </sec:authorize>
    
  <sec:authorize access="hasRole('ROLE_COURIER')">
     <div class="mainMenu">
	    <a href="${pageContext.request.contextPath}/joblist">My Jobs</a>
     </div> 
  </sec:authorize>

</sec:authorize>

<div class="mainMenu">
	<a href="${pageContext.request.contextPath}/information">Information</a>
</div>