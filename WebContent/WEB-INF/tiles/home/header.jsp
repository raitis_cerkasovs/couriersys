<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="help" style="height: 200px;">Site is tested with Safari 6.0.7. Microsoft Mozilla has few known issues.</div>

<sec:authorize access="isAuthenticated()">

  <!-- Output are dependent of user's authentication -->
  Hello <sec:authentication property="principal.username" />! 

  You are authorized as a 
  <sec:authorize access="hasRole('ROLE_COURIER')">
    courier.
  </sec:authorize>
  <sec:authorize access="hasRole('ROLE_CUSTOMER')">
    customer.
  </sec:authorize>

  &nbsp;

  <a href="<c:url value='/editaccount?un=${pageContext.request.userPrincipal.name}' />">Edit Account &nbsp;</a>
  <a href="<c:url value="/j_spring_security_logout" />">Logout</a> &nbsp;
</sec:authorize>

<sec:authorize access="!isAuthenticated()">
  <a href="<c:url value="/login" />">Login</a>&nbsp;    
</sec:authorize>

  

