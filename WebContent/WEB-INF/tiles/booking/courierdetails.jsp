<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="help">
<p>Courier's details and his feedbacks.</p>
<p>"Bookings" - link to return to the bookings page (at the bottom).</p>
</div>

<!-- Outut courier details from passed parameter. Followed by form to add feedback -->

<h3>Courier details</h3>

<table>

<tr>
 <td>Name:</td>
 <td style="text-align: left; color:gray;"><p>${courier.user.name}&nbsp;${courier.user.surname}&nbsp;</p>
</td>
</tr>

<tr>
 <td>Details:</td>
 <td style="text-align: left; color:gray;"><p>tel:&nbsp;${courier.user.phone},&nbsp;email:&nbsp;${courier.user.email}&nbsp;</p>
</td>
</tr>

</table>


<h3>Feedbacks:</h3>

<table>

<c:forEach var="feedback" items="${feedbacks}">
<tr>
 <td style="text-align: left;"><span style="font-size: small;">${feedback.created}</span><br>
 <p style="padding:20px; border: 1px solid brown;"><c:out value="${feedback.text}" ></c:out></p><br></td>
</tr>
</c:forEach>

</table>

<div class="mainMenu"><a style="text-align: left;" href="<c:url value="/availability" />">Bookings</a></div>
