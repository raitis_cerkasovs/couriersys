<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="help">
<p>The known issue with MOZILLA FIREFOX, it is NOT updating drop-boxes!!!</p>
<p>Currently working couriers are represented on the map and listed from the right side. Availability is presented with the relative icon in the couriers list. To view courier's details click on the relative icon next to the courier's name in the courier's list.</p>
<p>At the bottom of the page there are presented collection and delivery addresses entered by the registered user. "edit" - edit address, "delete" - remove address from the list, "Create new Collection Address" or "Create new Delivery Address" - add new addresses to the list.</p>
<p>Main booking area is within the bold red square. It is a two step process to book a courier. You have to select a collection address from related drop-box, delivery address from related drop-box, a courier from related drop-box and press "Book"</p>
<p>"view" - to explore existing job, "remove" - remove job from the list, "feedback" - leave feedback for a courier who carried the related job.</p>
</div>

<style>
#map_canvas {
	width: 500px;
	height: 400px;
	float: left;
}

td {
	padding: 3px;
	color: grey;
	text-decoration: none;
	transition: color 0.5s linear;
	-moz-transition: color 0.5s linear;
	-webkit-transition: color 0.5s linear;
	-o-transition: color 0.5s linear;
}

th {
	padding: 3px;
	text-align: left;
}

.green {
    color: green;
}

.red {
    color: red;
}

.yellow {
    color: yellow;
}

.brown {
    color: brown;
}

</style>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js"></script>

<!-- JS to disable job booking if sendto or sendfrom address is missing-->
<script type="text/javascript">

function blockJobCreation() {
	$('#validjob').show();
	$('#tobook').attr("disabled", true);
}

</script>

<!-- Top -->
<h4>Move mouse over courier's icon on the map.<br>Contact information and feedbacks are under "Info" icon in the list.</h4>

<!-- div for the Map -->
<div id="map_canvas"></div>





<!-- Couriers table (populated with JS) -->
<div style="border: 1px solid blue; margin-left: 520px; height: 400px;">
<h3>Available Couriers</h3>
<table id="courierstable">
<tr><th>Info</th><th>Vacant</th><th>Nickname</th></tr>
</table>
</div>





<!--  Booked jobs table-->
<div class="availability" style="border:5px solid red;">
<h3>My Booked Jobs</h3>

<table>
<tr><th></th><th class="jobs">Collection Address</th><th class="jobs">Delivery Address</th><th class="jobs">Courier</th><th class="jobs">Job Status</th><th class="jobs">Created</th></tr>

<c:forEach var="jobs" items="${jobs}">
<tr>
<td>
  <a style="font-size:11px;" href="<c:url value='/editjob?id=${jobs.id}'/>">view</a>
    
  <a style="font-size:11px;" href="<c:url value='/removejob?id=${jobs.id}'/>">remove</a>   
    
  <a style="font-size:11px;" href="<c:url value='/addfeedback?jid=${jobs.id}&cuid=${jobs.courier.username}&clid=${jobs.client.username}'/>">feedback</a>
</td>
<td><c:out value="${jobs.sendfrom.name}"></c:out></td>  
<td><c:out value="${jobs.sendto.name}"></c:out></td>  
<td><c:out value="${jobs.courier.username}"></c:out></td>
<td id="${jobs.id}" style="font-weight: bold;"><c:out value="${jobs.status}"></c:out></td>
<td style="font-size: small;"><c:out value="${jobs.created}"></c:out></td>
</tr>
</c:forEach>


<tr><th></th><th colspan="4" class="jobs red"><h3>New Job</h3></th></tr>
<tr><th></th><th class="jobs">Collection Addresses:</th><th class="jobs">Delivery Addresses:</th><th class="jobs">Available Couriers:</th></tr>


<!-- Book a joob form -->
<sf:form method="POST" commandName="job" action="${pageContext.request.contextPath}/savejob">


<tr> 

<td></td>
<td>
<select name="sendfromselect">
   <c:forEach items="${sendfrom}" var="sendfrom">
       <option value="${sendfrom.id}" label="${sendfrom.name}" />
    </c:forEach>
</select>
</td>

<td>
<select name="sendtoselect">
   <c:forEach items="${sendto}" var="sendto">
       <option value="${sendto.id}" label="${sendto.name}" />
    </c:forEach>
</select>
</td>

<td>
<select name="couriersselect">
   <c:forEach items="${couriers}" var="couriers">
       <option value="${couriers.username}" label="${couriers.username}" />
    </c:forEach>
</select>
</td>

<td><input id="tobook" class="submit" value="To Book" type="submit"></td>

</tr>
</sf:form>
</table>

<span id="validjob" style="color:red; display: none;">To create job you must have at least one "collection" and at least one "delivery" address.</span>

</div>








<!-- Collection address table -->

<div class="availability" style="border:1px solid green; ">
<h3>My Collection Addresses</h3>

<c:if test="${empty sendfrom}"><script type="text/javascript">blockJobCreation();</script></c:if>

<table>
<tr><th></th><th>Name</th><th>Postcode</th><th>Address</th></tr>
<c:forEach var="sendfrom" items="${sendfrom}">
<tr>
<td>
  <a style="font-size:11px;" href="<c:url value='/updateaddresscollect?id=${sendfrom.id}'/>">edit</a>
  <a style="font-size:11px;" href="<c:url value='/removeaddresscollect?id=${sendfrom.id}'/>">remove</a>
</td>
<td><c:out value="${sendfrom.name}"></c:out></td>
<td><c:out value="${sendfrom.postcode}"></c:out></td>  
<td><c:out value="${sendfrom.address1}"></c:out></td>  
<td><c:out value="${sendfrom.address2}"></c:out></td>  
</tr>
</c:forEach>
</table>
<div class="mainMenu"><a href="<c:url value='/updateaddresscollect'/>">Create Collection Address</a></div>
</div>


<!-- Delivery address table -->
<div class="availability"  style="border:1px solid brown;">
<h3>My Delivery Addresses</h3>

<c:if test="${empty sendto}"><script type="text/javascript">blockJobCreation();</script></c:if>

<table>
<tr><th></th><th>Name</th><th>Postcode</th><th>Address</th></tr>
<c:forEach var="sendto" items="${sendto}">
<tr>
<td>
  <a style="font-size:small;" href="<c:url value='/updateaddressdeliver?id=${sendto.id}'/>">edit</a>
  <a style="font-size:small;" href="<c:url value='/removeaddressdeliver?id=${sendto.id}'/>">remove</a>
</td>
<td><c:out value="${sendto.name}"></c:out></td>
<td><c:out value="${sendto.postcode}"></c:out></td>  
<td><c:out value="${sendto.address1}"></c:out></td>  
<td><c:out value="${sendto.address2}"></c:out></td>
</tr>
</c:forEach>
</table>
<div class="mainMenu"><a href="<c:url value='/updateaddressdeliver'/>">Create Destination Address</a></div>
</div>


<<<<<<< HEAD

<script type="text/javascript">
=======
>>>>>>> b89bddb8afb7658feab9618d5285d1f94795fb8d

<script type="text/javascript">
// initate map
var map_canvas = document.getElementById('map_canvas');
var map_options = {
	center : new google.maps.LatLng(51.495065,-0.082397),
	zoom : 10,
	mapTypeId : google.maps.MapTypeId.ROADMAP
    };

var marker = new Array();

var map = new google.maps.Map(map_canvas, map_options);

var infoWindow = new google.maps.InfoWindow();	 

var icons = {};
icons["car"] = "${pageContext.request.contextPath}/static/img/car40.png";
icons["van"] = "${pageContext.request.contextPath}/static/img/van40.png";
icons["bike"] = "${pageContext.request.contextPath}/static/img/bike40.png";
icons["push bike"] = "${pageContext.request.contextPath}/static/img/p_bike40.png";

function updateCouriers(data){
	
	  // update jobs status
	  for (var i = 0; i < data.jobs.length; i++) {
	         $("#" + data.jobs[i].id).html(data.jobs[i].status);
	         if (data.jobs[i].status == "Approved") {setGreen(data.jobs[i].id);}
		     if (data.jobs[i].status == "Rejected") {setRed(data.jobs[i].id);}
			 if (data.jobs[i].status == "Collected") {setYellow(data.jobs[i].id);}
		     if (data.jobs[i].status == "Delivered") {setBrown(data.jobs[i].id);}            
	  }

	  // remove all rows from couriers list
      $("#courierstable").find("tr:gt(0)").remove();
	  // remove all markers (can be tuned)
      deleteMarkers(data);


	 
	// update position on map
 	  for (var i = 0; i < data.couriers.length; i++) {
	    if (data.couriers[i].isworking) {

	    	      // set markers
			      latLng = new google.maps.LatLng(data.couriers[i].lat, data.couriers[i].lon); 

			      marker[data.couriers[i].username] = new google.maps.Marker({
			      position: latLng,
			      map: map,
 			      title: data.couriers[i].username,
			      icon: icons[data.couriers[i].type]
			      }); 

			   
                  // info window
    		      markerName = marker[data.couriers[i].username];	 
    		      info = "Hi, my name is <strong> " + data.couriers[i].username + "</strong>. <br> Find my feedbacks and contact information <br>under relative icon in the list.";	   
		   
    		      addInfoWindow(markerName, info, map);
			  
    		   
    		   // append elements to couriers list    	 		   
    		   if (data.couriers[i].isavailable) {
                   available = "<img width='20' height='20' src='${pageContext.request.contextPath}/static/img/true.jpg'>"; }
               else {
                   available = "<img width='20' height='20' src='${pageContext.request.contextPath}/static/img/false.png'>"; }
    		   
    		   feedback = "<a style='font-size:11px;' href='<c:url value='/courierdetails?uid=" + data.couriers[i].username + "'/>'><img width='20' height='20' src='${pageContext.request.contextPath}/static/img/Feedback2.jpg'></a>";
    		   
    		   $("#courierstable").append( "<tr><td>" + feedback + "</td><td>" + available + "</td><td>" + data.couriers[i].username + "</td></tr>");		   
	
       	   
       }
     }  
	
}
// info window to icons
function addInfoWindow(marker, message, map) {

<<<<<<< HEAD
function addInfoWindow(marker, message, map) {

=======
>>>>>>> b89bddb8afb7658feab9618d5285d1f94795fb8d
    var infoWindow = new google.maps.InfoWindow({
        content: message
    });

    google.maps.event.addListener(marker, 'mouseover', function () {
        infoWindow.open(map, marker);    
    });
    
    google.maps.event.addListener(marker, 'mouseout', function () {
        infoWindow.close();
    });
}
<<<<<<< HEAD

=======
// delete markers on iupdate
>>>>>>> b89bddb8afb7658feab9618d5285d1f94795fb8d
function deleteMarkers(data) {
	  for (var i = 0; i < data.couriers.length; i++) {
		  if (marker[data.couriers[i].username]) {
	         marker[data.couriers[i].username].setMap(null);
		  }
	  }
}

// repeate starts on-load
function onload() {
	updatePage();
	window.setInterval(updatePage, 5000);
}

// json update
function updatePage(){
	$.getJSON("<c:url value='/getcouriersjson'/>", updateCouriers);
}

// set colors to job statuses
function setGreen(id) {
    $("#" + id).removeClass("red");
    $("#" + id).removeClass("yellow");
    $("#" + id).removeClass("brown");
    $("#" + id).addClass("green");
}

function setRed(id) {
    $("#" + id).removeClass("green");
    $("#" + id).removeClass("yellow");
    $("#" + id).removeClass("brown");
    $("#" + id).addClass("red");
}

function setYellow(id) {
    $("#" + id).removeClass("red");
    $("#" + id).removeClass("green");
    $("#" + id).removeClass("brown");
    $("#" + id).addClass("yellow");
}

function setBrown(id) {
    $("#" + id).removeClass("red");
    $("#" + id).removeClass("yellow");
    $("#" + id).removeClass("green");
    $("#" + id).addClass("brown");
}

//on-load
$(document).ready(onload);

</script>
