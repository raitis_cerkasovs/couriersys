<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<div class="help">
<p>Booked courier's location on the map.</p>
<p>Optional fields after evaluation can be added - a price offer, message to the courier, etc. </p>
<p>After pressing "Save and Send", a new job or the changes, will appear on users list and will be sent to the courier.</p>
</div>

<style>
#map_canvas {
	width: 500px;
	height: 400px;
	margin: 0 auto;
	
}

</style>

<h3>Additional job details</h3>

<p>Here more fields will be added after initial evaluation. Both current fields are optional.</p>

<sf:form method="POST" action="${pageContext.request.contextPath}/updatejob" commandName="job">

<sf:hidden path="id"/>
<sf:hidden path="status"/>

<input type="hidden" name="clientx" id="clientx" value="${clientx }" class="com.project.dao.Client" />


<!-- Info table -->
<table>

<tr>
 <td>Collection:</td>
 <td style="text-align: left; color:gray;">${job.sendfrom.name},&nbsp;${job.sendfrom.address1}&nbsp;${job.sendfrom.address2},&nbsp;${job.sendfrom.postcode}</td>
</tr>

<tr>
 <td>Delivery:</td>
 <td style="text-align: left;color:gray;">${job.sendto.name},&nbsp;${job.sendto.address1}&nbsp;${job.sendto.address2},&nbsp;${job.sendto.postcode}</td>
</tr>

<tr>
 <td>Courier:</td>
 <td style="text-align: left;color:gray;">${job.courier.user.name}&nbsp;${job.courier.user.surname},&nbsp;tel:&nbsp;${job.courier.user.phone},&nbsp;&nbsp;email:&nbsp;${job.courier.user.email}&nbsp;</td>
</tr>

<tr>
 <td>Price offer:</td>
 <td style="text-align: left;"><sf:input path="price" type="text"></sf:input></td>
</tr>

<tr>
 <td>Notes:</td>
 <td style="text-align: left;"><sf:textarea  rows="4" cols="50" path="notes" type="text"></sf:textarea></td>
</tr>



<tr> <td></td><td><input class="submit" value="Save and Send" type="submit"></td></tr>
</table>
</sf:form>


<p>${job.courier.user.name}&nbsp;${job.courier.user.surname}&nbsp;location</p>

<div id="map_canvas"></div>

<script src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Google map - replica from "availability.jsp" file, apart of using only one marker -->

<script type="text/javascript">

var map_canvas = document.getElementById('map_canvas');
var map_options = {
	center : new google.maps.LatLng(51.495065,-0.082397),
	zoom : 10,
	mapTypeId : google.maps.MapTypeId.ROADMAP
    };
    
var map = new google.maps.Map(map_canvas, map_options);

var marker;

var username = "${job.courier.username}";

var infoWindow = new google.maps.InfoWindow();	 

var icons = {};
icons["car"] = "${pageContext.request.contextPath}/static/img/car40.png";
icons["van"] = "${pageContext.request.contextPath}/static/img/van40.png";
icons["bike"] = "${pageContext.request.contextPath}/static/img/bike40.png";
icons["push bike"] = "${pageContext.request.contextPath}/static/img/p_bike40.png";

function updateCouriers(data){ 

	  // remove all markers (can be tuned)
      deleteMarkers(data);
 
	  // update position on map	 
	  if (data.courier.isworking) {
                  
	    	      // set marker
			      latLng = new google.maps.LatLng(data.courier.lat, data.courier.lon); 

			      marker = new google.maps.Marker({
			      position: latLng,
			      map: map,
 			      title: data.courier.username,
			      icon: icons[data.courier.type]
			      }); 
			   
                  // info window
    		      markerName = marker;	 
    		      info = "Hi, my name is <strong> " + username + "</strong>. <br> Find my feedbacks and contact information <br>is under relative icon in the list.";	   
		   
    		       (function(markerName, data) {
 		 		      google.maps.event.addListener(markerName, "mouseover", function(e) {
 		 		        infoWindow.setContent(info);
 		 		        infoWindow.open(map, markerName);
 		 		      });
                   })(markerName, data);
       //	   
       } 
	
}

function deleteMarkers(data) {
	if (marker) {
	  marker.setMap(null);
	}
}		

function onload() {
	updatePage();
	window.setInterval(updatePage, 5000);
}

function updatePage(){
	$.getJSON("<c:url value='/getcourierjson?uid=" + username + "'/>", updateCouriers);
}

$(document).ready(onload);

</script>

<div class="mainMenu"><a style="text-align: left;" href="<c:url value="/availability" />">Bookings</a></div>
