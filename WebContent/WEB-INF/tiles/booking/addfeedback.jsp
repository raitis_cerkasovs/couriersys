<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="help">
<p>Add feedback to a courier.</p>
</div>

<h3>Create feedback</h3>


<sf:form method="POST" action="${pageContext.request.contextPath}/savefeedback" commandName="feedback">


<sf:hidden path="job" value="${job}"/>
<sf:hidden path="courier" value="${courier}"/>
<sf:hidden path="client" value="${client}"/>

<table>

<tr>
 <td>Feedback for courier <span style="color:gray;">${courier}:</span></td>
</tr>

<tr>
 <td style="text-align: left;"><sf:textarea  rows="4" cols="50" path="text" type="text"></sf:textarea><br />
 <div class="error"><sf:errors path="text"></sf:errors></div></td>
</tr>

<tr>
 <td><input class="submit" value="Add" type="submit">
 </td>
</tr>

</table>

</sf:form>

<div class="mainMenu"><a style="text-align: left;" href="<c:url value="/availability" />">Bookings</a></div>

