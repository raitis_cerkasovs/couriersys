<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="help">
<p>Create or update delivery address.</p>
<p>Address line one, phone and a postcode have validation.</p>
</div>

<sf:form method="POST" action="${pageContext.request.contextPath}/saveaddressdeliver" commandName="sendto">

<sf:hidden path="id"/>
<sf:hidden path="visible"/>

<table>

<tr> <td>Name:</td><td><sf:input path="name" type="text"></sf:input><br />
<div class="error"><sf:errors path="name"></sf:errors></div>
</td></tr>

<tr> <td>Address line one:</td><td><sf:input path="address1" type="text"></sf:input><br />
<div class="error"><sf:errors path="address1"></sf:errors></div>
</td></tr>

<tr> <td>Address line two:</td><td><sf:input path="address2" type="text"></sf:input><br />
<div class="error"><sf:errors path="address2"></sf:errors></div>
</td></tr>


<tr> <td>Postcode:</td><td><sf:input path="postcode" type="text"></sf:input><br />
<div class="error"><sf:errors path="postcode"></sf:errors></div>
</td></tr>

<tr> <td>Notes:</td><td><sf:textarea path="notes" type="text"></sf:textarea><br />
<div class="error"><sf:errors path="notes"></sf:errors></div>
</td></tr>

<tr> <td></td><td><input class="submit" value="Save" type="submit"></td></tr>
</table>
</sf:form>
