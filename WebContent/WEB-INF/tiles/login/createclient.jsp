<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="help">
<p>Client related information. Will be implemented after initial evaluation.</p>
</div>

<h3>Step 3 of 4 </h3>

<p> Please complete clients related information.</p>

<sf:form method="POST" commandName="client" >

<!-- Spring web-flow related parameters. Value is a name of commited action -->
<input type="hidden" name="_flowExecutionKey" value="${(flowExecutionKey)}" />
<input type="hidden" name="_eventId" value="createclient" />

<table>

  <tr> 
    <td>Clients related information:</td>
    <td>Can be added after initial evaluation. Potentially - a payment method, a company or private client, etc.</td>
  </tr>

<tr> <td></td><td><input class="submit" value="Step 4" type="submit"></td></tr>
</table>
</sf:form>