<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="help">
<p>The known issue with MOZILLA FIREFOX, it is NOT updating drop-boxes!!!</p>
<p>Edit personal details.</p>
</div>

<h3>Edit profile:</h3>
 
<!-- action on "updateaccount" activity -->
<sf:form method="POST" action="${pageContext.request.contextPath}/updateaccount" commandName="user">

<sf:hidden path="password"/>
<sf:hidden path="iscourier"/>
<sf:hidden path="enabled"/>

<sec:authorize access="hasRole('ROLE_COURIER')">
<sf:hidden path="courier.username"/>
</sec:authorize>

<table>

<tr> <td>Username:</td><td><sf:input path="username" type="text"></sf:input><br />
<div class="error"><sf:errors path="username"></sf:errors></div>
</td></tr>

<tr> <td>Name:</td><td><sf:input path="name" type="text"></sf:input><br />
<div class="error"><sf:errors path="name"></sf:errors></div>
</td></tr>

<tr> <td>Surname:</td><td><sf:input path="surname" type="text"></sf:input><br />
<div class="error"><sf:errors path="surname"></sf:errors></div>
</td></tr>

<tr><td>Email:</td><td><sf:input path="email" type="text"></sf:input><br />
<div class="error"><sf:errors path="email"></sf:errors></div>
</td></tr>



<tr> <td>Phone:</td><td><sf:input path="phone" type="text"></sf:input><br />
<div class="error"><sf:errors path="phone"></sf:errors></div>
</td></tr>

<tr> <td>Address 1:</td><td><sf:input path="address1" type="text"></sf:input><br />
<div class="error"><sf:errors path="address1"></sf:errors></div>
</td></tr>

<tr> <td>Address 2</td><td><sf:input path="address2" type="text"></sf:input><br />
<div class="error"><sf:errors path="address2"></sf:errors></div>
</td></tr>

<tr> <td>Postcode:</td><td><sf:input path="postcode" type="text"></sf:input><br />
<div class="error"><sf:errors path="postcode"></sf:errors></div>
</td></tr>



<sec:authorize access="hasRole('ROLE_COURIER')">
<tr> 
    <td>Vehicle type:</td>
     <td>      
       <sf:select path="courier.type"> 
        <sf:option value="bike">Bike</sf:option>
        <sf:option value="push bike">Push bike</sf:option>
        <sf:option value="car">Car</sf:option>
        <sf:option value="van">Van</sf:option> 
      </sf:select>
      <br />
      <div class="error"><sf:errors path="courier.type"></sf:errors></div>
    </td>
</tr>
</sec:authorize>



<tr> <td></td><td><input class="submit" value="Update" type="submit"></td></tr>
  
</table>

</sf:form>

<div class="mainMenu"><a style="text-align: left;" href="<c:url value="/" />">Home</a></div>

