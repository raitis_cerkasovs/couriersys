<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="help">
<p>The four step web flow. All fields has a validation.</p>
<p>Validation for an existing user is missing.</p>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
 
<script type="text/javascript">

function onLoad() {
	$('#pw').keyup(passwordMutch);
    $('#cpw').keyup(passwordMutch);
    $('#details').submit(passwordMutch);
}

function passwordMutch() {
	var pw = $('#pw').val();
	var cpw = $('#cpw').val();
	
	if (pw.length > 7 || cpw.length > 7) {
		
		if (pw == cpw) {
			$('#ecp').text("<fmt:message key='MatchPasswords.user.password' />");
			return true;
		} else {
			$('#ecp').text("<fmt:message key='UnmatchPasswords.user.password' />");
			return false;
		}		
	}
}

$(document).ready(onLoad);

</script>
<h3>Step 1 of 4 </h3>

<p> Please enter your name and access credentials.</p>
 
<sf:form id="details" method="POST" commandName="user">

<input type="hidden" name="_flowExecutionKey" value="${(flowExecutionKey)}" />
<input type="hidden" name="_eventId" value="createuser" />

<table>

<tr> <td>Username:</td><td><sf:input path="username" type="text"></sf:input><br />
<div class="error"><sf:errors path="username"></sf:errors></div>
</td></tr>

<tr> <td>Name:</td><td><sf:input path="name" type="text"></sf:input><br />
<div class="error"><sf:errors path="name"></sf:errors></div>
</td></tr>

<tr> <td>Surname:</td><td><sf:input path="surname" type="text"></sf:input><br />
<div class="error"><sf:errors path="surname"></sf:errors></div>
</td></tr>

<tr><td>Email:</td><td><sf:input path="email" type="text"></sf:input><br />
<div class="error"><sf:errors path="email"></sf:errors></div>
</td></tr>

<tr><td>Password:</td><td><sf:input id="pw" path="password" type="password"></sf:input><br />
<div class="error"><sf:errors path="password"></sf:errors></div>
</td></tr>

<tr><td>Confirm password:</td><td><input id="cpw" name="confirmpassword" type="password" />
<br /><div class="error" id="ecp"></div>
</td></tr>

<tr> <td></td><td><input class="submit" value="Step 2" type="submit"></td></tr>
</table>
</sf:form>
