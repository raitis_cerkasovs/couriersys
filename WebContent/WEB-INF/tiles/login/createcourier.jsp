<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="help">
<p>Courier's related information. </p>
<p>Additional fields will be added after initial evaluation</p>
</div>

<h3>Step 3 of 4 </h3>

<p> Please complete couriers related information.</p>

<sf:form method="POST" commandName="courier">

<!-- Spring web-flow related parameters. Value is a name of commited action -->
<input type="hidden" name="_flowExecutionKey" value="${(flowExecutionKey)}" />
<input type="hidden" name="_eventId" value="createcourier" />

<table>
  <tr> 
    <td>Vehicle type:</td>
    <td>
      <select id="type" name="type"> 
       <option value="bike">Bike</option>
       <option value="push bike">Push bike</option>
       <option value="car">Car</option>
       <option value="van">Van</option> 
      </select>
      <br />
<div class="error"><sf:errors path="type"></sf:errors></div>
</td></tr>

  <tr> 
    <td>Other:</td>
    <td>...</td>
  </tr>

<tr> <td></td><td><input class="submit" value="Step 4" type="submit"></td></tr>
</table>
</sf:form>