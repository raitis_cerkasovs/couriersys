<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="help">
<p>Address line one and the postcode have validation.</p>
</div>

<h3>Step 2 of 4 </h3>

<p> Please enter your phone and address.</p>
 
<sf:form method="POST" commandName="user">

<!-- Spring web-flow related parameters. Value is a name of commited action -->
<input type="hidden" name="_flowExecutionKey" value="${(flowExecutionKey)}" />
<input type="hidden" name="_eventId" value="createuser2" />

<table>

<tr> <td>Phone:</td><td><sf:input path="phone" type="text"></sf:input><br />
<div class="error"><sf:errors path="phone"></sf:errors></div>
</td></tr>

<tr> <td>Address:</td><td><sf:input path="address1" type="text"></sf:input><br />
<div class="error"><sf:errors path="address1"></sf:errors></div>
</td></tr>

<tr> <td>Address:</td><td><sf:input path="address2" type="text"></sf:input><br />
<div class="error"><sf:errors path="address2"></sf:errors></div>
</td></tr>

<tr> <td>Postcode:</td><td><sf:input path="postcode" type="text"></sf:input><br />
<div class="error"><sf:errors path="postcode"></sf:errors></div>
</td></tr>

<tr> <td></td><td><input class="submit" value="Step 3" type="submit"></td></tr>
</table>
</sf:form>