<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<h3>Step 4 of 4 </h3>

<p> Please enter your bank details.</p>

<sf:form method="POST" commandName="user">

<!-- Spring web-flow related parameters. Value is a name of commited action -->
<input type="hidden" name="_flowExecutionKey" value="${(flowExecutionKey)}" />
<input type="hidden" name="_eventId" value="createbank" />
<table>
  <tr> 
    <td>Not going to be implemented in scope of this project.</td>    
    <td></td>
  </tr>

<tr> <td></td><td><input class="submit" value="Finish" type="submit"></td></tr>
</table>
</sf:form>