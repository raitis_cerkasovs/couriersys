package com.project.controllers;

import org.springframework.dao.DataAccessException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author raitis
 *
 * Class to perform a behavior in case of exception
 */
@ControllerAdvice
public class ErrorHandler {
	
	
	/**
	 * @param ex - exception
	 * @return
	 * 
	 * Return "error" view if error ocure
	 */
	@ExceptionHandler(DataAccessException.class)
	public String handleDatabaseException(DataAccessException ex) {
		ex.printStackTrace();
		return "error";
		
	}	
	
	/**
	 * @param ex - exception
	 * @return
	 * 
	 * Return "accessdenied" view if accessed forbidden resource
	 */
	@ExceptionHandler(AccessDeniedException.class)
	public String handleAccessException(AccessDeniedException ex) {
		return "accessdenied";
		
	}

}
