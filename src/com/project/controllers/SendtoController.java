package com.project.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.project.dao.Sendto;
import com.project.service.AddressesService;
import com.project.service.BookingsService;
import com.project.service.UsersService;

/**
 * @author raitis
 *
 * All actions with "sendto" address
 */
@Controller
public class SendtoController {
	
	
	/**
	 * Add services
	 */
	@Autowired
	private BookingsService bookingsService;	
	
	@Autowired
	private UsersService usersService;	
	
	@Autowired
	private AddressesService addressesService;
	
	
	/**
	 * @param model - pass sendfrom parameter
	 * @param id - sendto  address id
	 * @return
	 * 
	 * Return new Sendto address or, if suplied id, return existing sendto address to edit
	 */
	@RequestMapping("/updateaddressdeliver")
	 public String showUpdateDeliverAddressForm(Model model, @RequestParam(value = "id", required=false) Integer id) {
		
		if (id == null) {
			 Sendto sendto = new Sendto();
			 sendto.setVisible(true);
		     model.addAttribute("sendto", sendto);
		}
		else {
			model.addAttribute("sendto", addressesService.getSendto(id));
		}
	   
		return "updateaddressdeliver";
	 }
	
	
	/**
	 * @param model - not used
	 * @param id - sendto address id
	 * @return
	 * 
	 * Change sendto address "setVisible" field to false
	 */
	@RequestMapping("/removeaddressdeliver")
	 public String removeAddressDeliver(Model model, @RequestParam("id") Integer id) {
				
        Sendto sendto = addressesService.getSendto(id);        
        sendto.setVisible(false);	        
        addressesService.createOrUpdateAddressDeliver(sendto);	        
		return "redirect:availability";	
	 }
		
	
	/**
	 * @param model - not used
	 * @param id - sendto address id
	 * @return
	 * 
	 * Replaced by removeAddressCollect(). Can be used by administrator for maintenance
	 */
	@RequestMapping("/deleteaddressdeliver")
	 public String deleteAddressDeliver(Model model, @RequestParam("id") Integer id) {
		
		addressesService.deleteSendto(id);

		return "redirect:availability";
	 }	
	
	
	/**
	 * @param model - not used
	 * @param sendfrom - passed from a view
	 * @param result - validator error list
	 * @param principal
	 * @return
	 * 
	 * Save sendto address
	 */
	@RequestMapping("/saveaddressdeliver")
	 public String saveAddressDeliver(Model model, @Valid Sendto sendto, BindingResult result, Principal principal) {
		
		if (result.hasErrors()) {
			return "updateaddressdeliver";
		}		
		
		sendto.setUsername(principal.getName());
		addressesService.createOrUpdateAddressDeliver(sendto);
		
		return "redirect:availability";
		 
	 }
	
}
