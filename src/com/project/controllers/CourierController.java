package com.project.controllers;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.project.dao.Courier;
import com.project.dao.Feedback;
import com.project.dao.Job;
import com.project.service.AddressesService;
import com.project.service.BookingsService;
import com.project.service.UsersService;


/**
 * @author raitis
 * 
 * Class to manipulate with Couriers's related functionality
 */
@Controller
public class CourierController {
	
	/**
	 * Register services
	 */
	@Autowired
	private BookingsService bookingsService;	
	
	@Autowired
	private UsersService usersService;	
	
	@Autowired
	private AddressesService addressesService;
	
	
	/**
	 * @param model - pass Courier model and Feedbacks list
	 * @param username - Courier's username
	 * @return
	 * 
	 * Get courier details and his feedbacks after clicking on its icon
	 */
	@RequestMapping("/courierdetails")
	 public String courierDetails(Model model, @RequestParam("uid") String username) {	
		
		Courier courier = bookingsService.getCourier(username);		
		List<Feedback> feedbacks = bookingsService.getFeedbacks(username);
		
		model.addAttribute("courier", courier);
		model.addAttribute("feedbacks", feedbacks);
		
		return "courierdetails";
	}
	
	
  	/**
  	 * @param model - pass attributes
  	 * @param jobid - job id
  	 * @param courierid - courier id
  	 * @param clientid - client id
  	 * @return
  	 * 
  	 * Generate feedback, set client, courier, job
  	 */
  	@RequestMapping("/addfeedback")
	 public String addFeedback(Model model,  
	           @RequestParam("jid") Integer jobid,
               @RequestParam("cuid") String courierid,	
	           @RequestParam("clid") String clientid) {	
		
		Feedback feedback = new Feedback();
		
		model.addAttribute("feedback", feedback);
		model.addAttribute("job", jobid);
		model.addAttribute("courier", courierid);
		model.addAttribute("client", clientid);
				
	   return "addfeedback";
	 }
  	
  	
  	/**
  	 * @param feedback
  	 * @return
  	 * 
  	 * Save the feedback
  	 */
  	@RequestMapping("/savefeedback")
	 public String addFeedback(Feedback feedback){ 
  		
     	java.util.Date date= new java.util.Date();
  		feedback.setCreated(new Timestamp(date.getTime()));
  		
  		bookingsService.createOrUpdateFeedback(feedback);
  		
  		return "redirect:availability";
  	}
  	
  	
	
	/**
	 * @param principal - registered user
	 * @return
	 * 
	 * Generate JSON list with Couriers returned from getAllWorkingCouriers() service
	 */
	@RequestMapping(value="/getcouriersjson", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> getCouriersJson(Principal principal){
		
		List<Courier> couriers = bookingsService.getAllWorkingCouriers();
        List<Job> jobs= bookingsService.getJobs(principal.getName());			
		        
        Map<String, Object> data = new HashMap<String, Object>();
        
        data.put("couriers", couriers);
        data.put("number", couriers.size());
        data.put("jobs", jobs);

		return data;
	}  	
  	
	
	/**
	 * @param username - Courier's username
	 * @return
	 * 
	 * Get Courier data formated as JSON 
	 */
	@RequestMapping(value="/getcourierjson", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> getCourierJson(@RequestParam("uid") String username){
		
		Courier courier = bookingsService.getCourier(username);
		        
        Map<String, Object> data = new HashMap<String, Object>();
        
        data.put("courier", courier);

		return data;
	}

}
