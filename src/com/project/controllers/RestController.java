package com.project.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.project.dao.Courier;
import com.project.dao.Job;
import com.project.dao.Sendfrom;
import com.project.dao.Sendto;
import com.project.dao.User;
import com.project.service.AddressesService;
import com.project.service.BookingsService;
import com.project.service.UsersService;


/**
 * @author raitis
 *
 * All REST services
 */
@Controller
public class RestController {
	
	
	/**
	 * Add services
	 */
	@Autowired
	private BookingsService bookingsService;	
	
	@Autowired
	private UsersService usersService;	
	
	@Autowired
	private AddressesService addressesService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
		
	
	/**
	 * @param username - courier's username
	 * @param lat - latitude from android positioning
	 * @param lon longitude from android positioning
	 * @return
	 * 
	 * RESTful service, gathering courier's usename, latitude and longitude, updating them in database,
	 * and returning JSON data with courier's jobs list
	 */
	@RequestMapping(value="/getjobsjson", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> getjobsJson(@RequestParam("uid") String username,
			                               @RequestParam("lat") String lat,
			                               @RequestParam("lon") String lon){
		
		Courier courier = bookingsService.getCourier(username);
		
		courier.setLat(Float.valueOf(lat));
		courier.setLon(Float.valueOf(lon));		
		
		courier.setIsworking(true);
		
		usersService.saveOrUpdateCourier(courier);
		
        List<Job> jobs= bookingsService.getCourierJobsJson(username);			
		        
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("jobs", jobs);

		return data;
	}  		
	
	
	/**
	 * @param id - job id
	 * @return
	 * 
	 * RESTful service, returning JSON with all job related information collect with job id parameter 
	 */
	@RequestMapping(value="/getdetailsjson", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> getDetailsJson(@RequestParam("id") Integer id){
		
		Sendto sendto = null;
		Sendfrom sendfrom = null;
		User user = null;
		
        Job job = bookingsService.getJob(id);
        
        if (job != null) {
           sendto = job.getSendto();
           sendfrom = job.getSendfrom();
           sendfrom = job.getSendfrom();
           user = usersService.getUser(job.getClientUsername());
        }
		        
        Map<String, Object> data = new HashMap<String, Object>();

        data.put("job", job);
        data.put("sendfrom", sendfrom);
        data.put("sendto", sendto);
        data.put("user", user);

		return data;
	}  	
	
	
	
	/**
	 * @param id - job id
	 * @param status - status
	 * @return
	 * 
	 * Retrieve job id and status as a GET parameters, update job's status and return JSON success 0 or 1
	 */
	@RequestMapping(value="/updatestatusjson", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> updateStatusJson(@RequestParam("id") Integer id, @RequestParam("status") String status) {
		
        Map<String, Object> data = new HashMap<String, Object>();
        
        Job job = bookingsService.getJob(id);

      
		if (job!= null) {
			
	        job.setStatus(status);
	        bookingsService.createOrUpdateJob(job);
	        
	        if(status.equals("Collected")) {
	            Courier courier = bookingsService.getCourier(job.getCourierUsername());
	            courier.setIsavailable(false);
	            usersService.saveOrUpdateCourier(courier);
	        }	
	        
	        if(status.equals("Delivered")) {
	            Courier courier = bookingsService.getCourier(job.getCourierUsername());
	            courier.setIsavailable(true);
	            usersService.saveOrUpdateCourier(courier);
	        }
	        
	        data.put("success", 1);
	        data.put("message", "Status updated");
			
		} else {
			
	        data.put("success", 0);
	        data.put("message", "Error ocured");
		}
		
        return data;
        
	}
	
	
	/**
	 * @param username - user username
	 * @param password - user password
	 * @return
	 * 
	 * RESTful service, retrieve's username and password as a GET parameters, make authentication,
	 * and return JSON success 0 or 1 accordingly
	 */
	@RequestMapping(value="/loginjson", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> loginJson(@RequestParam("uid") String username, @RequestParam("psw") String password) {
		
        Map<String, Object> data = new HashMap<String, Object>();
        
        User user = usersService.getUser(username);
        
        Courier courier = bookingsService.getCourier(username);
        
        if (user != null) {
        	if (user.isIscourier()) {    
                if (passwordEncoder.matches(password, user.getPassword())) {     
                   courier.setIsworking(true);
            	   usersService.saveOrUpdateCourier(courier);
	               data.put("success", 1);
	               return data;
	            }
        	}
        }

        data.put("success", 0);     
        return data;       
	}  	
	
	
	/**
	 * @param username -courier's username
	 * @return
	 * 
	 * RESTful service, retrieve courier by username and changes his isWorking value to false, therefore making him invisible
	 */
	@RequestMapping(value="/stopcourierjson", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> stopcourierjson(@RequestParam("uid") String username) {
		
        Map<String, Object> data = new HashMap<String, Object>();
        
       Courier courier = bookingsService.getCourier(username);
        
        if (courier != null) {
        	 courier.setIsworking(false);
        	 usersService.saveOrUpdateCourier(courier);
	         data.put("success", 1);
	         return data;
        }

        data.put("success", 0);     
        return data;       
	}  
	

}
