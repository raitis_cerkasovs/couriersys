package com.project.controllers;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.project.dao.Client;
import com.project.dao.Courier;
import com.project.dao.Job;
import com.project.dao.Sendfrom;
import com.project.dao.Sendto;
import com.project.dao.User;
import com.project.service.AddressesService;
import com.project.service.BookingsService;
import com.project.service.UsersService;

/**
 * @author raitis
 * 
 * Class to format an reformat the booking (job)
 */
@Controller
public class BookingController {
		
		/**
		 * Include services
		 */
		@Autowired
		private BookingsService bookingsService;	
		
		@Autowired
		private UsersService usersService;	
		
		@Autowired
		private AddressesService addressesService;
		
		
		/**
		 * @param model - pass parameters
		 * @param principal - registered user
		 * @return
		 * 
		 * Main function to generate console page. All user' addresses, jobs, working couriers
		 */
		@RequestMapping("/availability")
		 public String showAvailability(Model model, Principal principal) {
			
            List<User> couriers= usersService.getUsers("COURIER");			
            model.addAttribute("couriers", couriers);
            		
			List<Sendfrom> sendfrom = addressesService.getSendfrom(principal.getName());			
			model.addAttribute("sendfrom", sendfrom);	
			
			List<Sendto> sendto = addressesService.getSendto(principal.getName());			
			model.addAttribute("sendto", sendto);
	          
            List<Job> jobs= bookingsService.getJobs(principal.getName());			
            model.addAttribute("jobs", jobs);	
			
			model.addAttribute("job", new Job()); 
			
			return "availability";		 
		 }
	
		
		 /**
		 * @param id - job id
		 * @return
		 * 
		 * set "visible" to false
		 */
		@RequestMapping("/removejob")	
		 public String removeJob(@RequestParam("id") Integer id) {			
			if (id != null) {
			  	
			   Job job = bookingsService.getJob(id);		  
               job.setVisible(false);
				
               bookingsService.createOrUpdateJob(job);
				
			   return "redirect:availability";		 
			}		
			return "error";
		 }		
		
		 
		/**
		 * @param model - pass parameters
		 * @param job - job object from the form
		 * @param principal - registered user
		 * @param request - get parameters
		 * @return
		 * 
		 * Setting courier and addresses to the job to pass for updateJob()
		 */
		@RequestMapping("/savejob")
		 public String saveJob(Model model, Job job, Principal principal, HttpServletRequest request) {	
			
			String sendfromselect = request.getParameter("sendfromselect");
			int sendfromselectint = Integer.parseInt(sendfromselect);		
			Sendfrom sendfrom = addressesService.getSendfrom(sendfromselectint);	
			
			String sendtoselect = request.getParameter("sendtoselect");
			int sendtoselectint = Integer.parseInt(sendtoselect);		
			Sendto sendto = addressesService.getSendto(sendtoselectint);		
			
			String couriersselect= request.getParameter("couriersselect");
			Courier courier = bookingsService.getCourier(couriersselect);
			
			Client client = bookingsService.getClient(principal.getName());
			
			job.setSendto(sendto);
		    job.setCourier(courier);
	    	job.setClient(client);
	    	job.setSendfrom(sendfrom);	
	    	job.setStatus("Sent to Courier");	
				    	
		    model.addAttribute("job", job); 
		    
		    request.getSession().setAttribute("client", client); 
		    request.getSession().setAttribute("courier", courier); 
		    request.getSession().setAttribute("sendto", sendto);  
		    request.getSession().setAttribute("sendfrom", sendfrom);
	    				
			return "updatejob";			 
		 }		
	
		
		 /**
		 * @param model - pass job as a parameter
		 * @param id - job id
		 * @param request - set session attributes
		 * @return
		 * 
		 * Edit existing job, set attributes passed for updateJob()
		 */
		@RequestMapping("/editjob")	
		 public String editJob(Model model, @RequestParam("id") Integer id, HttpServletRequest request) {			
			if (id != null) {
			  	
			    Job job = bookingsService.getJob(id);
			  
				Sendfrom sendfrom = job.getSendfrom();
				Sendto sendto = job.getSendto();
				Client client = job.getClient();
				Courier courier = job.getCourier();  
				
				model.addAttribute("job", job);
				
				request.getSession().setAttribute("client", client);
				request.getSession().setAttribute("courier", courier);
				request.getSession().setAttribute("sendto", sendto);
				request.getSession().setAttribute("sendfrom", sendfrom);
				
			  return "updatejob";		 

			}			
			return "error";
		 }	
		
		 
		 
		/**
		 * @param model - not used
		 * @param job passed job object
		 * @param request - get session attributes
		 * @return
		 * 
		 * Get formed job from editJob() or saveJob() via attributes and save. Then destroy attributes
		 */
		@RequestMapping("/updatejob")
		 public String updateeJob(Model model, Job job, HttpServletRequest request) {			
			
			Client client = (Client) request.getSession().getAttribute("client");
			job.setClient(client);	
			
			Courier courier = (Courier) request.getSession().getAttribute("courier");	
			job.setCourier(courier);	
			
			Sendfrom sendfrom = (Sendfrom) request.getSession().getAttribute("sendfrom");	
			job.setSendfrom(sendfrom);		
			
			Sendto sendto = (Sendto) request.getSession().getAttribute("sendto");	
			job.setSendto(sendto);
			
			job.setVisible(true);
			
	    	java.util.Date date= new java.util.Date();
	   		job.setCreated(new Timestamp(date.getTime()));
			
			bookingsService.createOrUpdateJob(job);
		
			request.getSession().removeAttribute("client");
			request.getSession().removeAttribute("courier");
			request.getSession().removeAttribute("sendto");
    		request.getSession().removeAttribute("sendfrom");		
			
			return "redirect:availability";
		}
		
}
