package com.project.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.project.dao.Sendfrom;
import com.project.service.AddressesService;
import com.project.service.BookingsService;
import com.project.service.UsersService;


/**
 * @author raitis
 *
 * All actions with "sendfrom" address
 */
@Controller
public class SendfromController {
	
	
	/**
	 * Add DAO services
	 */
	@Autowired
	private BookingsService bookingsService;	
	
	@Autowired
	private UsersService usersService;	
	
	@Autowired
	private AddressesService addressesService;
	
	
	
	/**
	 * @param model - pass sendfrom parameter
	 * @param id - sendrom  address id
	 * @return
	 * 
	 * Return new Sendfrom address or, if suplied id, return existing sendfrom address to edit
	 */
	@RequestMapping("/updateaddresscollect")
	 public String showUpdateCollectAddressForm(Model model, @RequestParam(value = "id", required=false) Integer id) {
		
		if (id == null) {
			 Sendfrom sendfrom = new Sendfrom();
			 sendfrom.setVisible(true);
		     model.addAttribute("sendfrom", sendfrom);
		}
		else {
			model.addAttribute("sendfrom", addressesService.getSendfrom(id));
		}
	   
		return "updateaddresscollect";
	 }	
	
	
	/**
	 * @param model - not used
	 * @param id - sendfrom address id
	 * @return
	 * 
	 * Change sendfrom address "setVisible" field to false
	 */
	@RequestMapping("/removeaddresscollect")
	 public String removeAddressCollect(Model model, @RequestParam("id") Integer id) {
		
        Sendfrom sendfrom = addressesService.getSendfrom(id);        
        sendfrom.setVisible(false);	        
        addressesService.createOrUpdateAddressCollect(sendfrom);	        
		return "redirect:availability";
	 }	
	
	
	/**
	 * @param model - not used
	 * @param id - sendfrom address id
	 * @return
	 * 
	 * Replaced by removeAddressCollect(). Can be used by administrator for maintenance
	 */
	@RequestMapping("/deleteaddresscollect")
	 public String deleteAddressCollect(Model model, @RequestParam("id") Integer id) {
		
		addressesService.deleteSendfrom(id);

		return "redirect:availability";
	 }	
	
	
	/**
	 * @param model - not used
	 * @param sendfrom - passed from a view
	 * @param result - validator error list
	 * @param principal
	 * @return
	 * 
	 * Save sendfrom address
	 */
	@RequestMapping("/saveaddresscollect")
	 public String saveAddressCollect(Model model, @Valid Sendfrom sendfrom, BindingResult result, Principal principal) {
		
		if (result.hasErrors()) {
			return "updateaddresscollect";
		}		
		
		sendfrom.setUsername(principal.getName());
		addressesService.createOrUpdateAddressCollect(sendfrom);
		
		return "redirect:availability";
		 
	 }	

}
