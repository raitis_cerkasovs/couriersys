package com.project.controllers;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.project.dao.Job;
import com.project.service.BookingsService;


/**
 * @author raitis
 * 
 * Controller for initial pages - home, information, "My Jobs" for courier profile
 */ 
@Controller
public class HomeController {
	
	/**
	 * Initate service
	 */
	@Autowired
	private BookingsService bookingsService;


	/**
	 * @return
	 * 
	 * First initial page controller
	 */
	@RequestMapping("/")
	 public String showHome() {
	
		return "home";
		 
	 }	
	
	/**
	 * @return
	 * 
	 * Return view for "information" page
	 */
	@RequestMapping("/information")
	 public String showInfo() {
	
		return "information";
		 
	 }		
	
	
	/**
	 * @param model - pass parameters
	 * @param principal - registered user
	 * @return
	 * 
	 * List of commited jobs for courier profile
	 */
	@RequestMapping("/joblist")
	 public String jobList(Model model, Principal principal) {
		
		List<Job> jobs = bookingsService.getCourierJobs(principal.getName());	
		model.addAttribute("jobs", jobs);
	
		return "joblist";
		 
	 }	

}
