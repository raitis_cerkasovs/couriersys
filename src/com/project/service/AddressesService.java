package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.dao.Sendfrom;
import com.project.dao.SendfromDAO;
import com.project.dao.Sendto;
import com.project.dao.SendtoDAO;

/**
 * @author raitis
 * 
 * Wrapper class for DAO sendto and sendfrom functions
 */
@Service("addressesService")
public class AddressesService {
		
	@Autowired
	private SendtoDAO sendtoDao;	
	
	@Autowired
	private SendfromDAO sendfromDao;	
	

	public void createOrUpdateAddressDeliver(Sendto sendto) {
        sendtoDao.saveOrUpdate(sendto);		
	}

	public void createOrUpdateAddressCollect(Sendfrom sendfrom) {
        sendfromDao.saveOrUpdate(sendfrom);				
	}
	
	public List<Sendfrom> getSendfrom(String username) {
        return sendfromDao.getSendfrom(username);		
	}

	public List<Sendto> getSendto(String username) {
        return sendtoDao.getSendto(username);		
	}
	
	public Sendfrom getSendfrom(Integer id) {
        return sendfromDao.getSendfrom(id);		
	}

	public Sendto getSendto(Integer id) {
        return sendtoDao.getSendto(id);		
	}

	public void deleteSendfrom(Integer id) {
        sendfromDao.delete(id);		
	}

	public void deleteSendto(Integer id) {
        sendtoDao.delete(id);			
	}

	public List<Sendto> getAllSendto() {
        return sendtoDao.getSendto();		
	}
}
