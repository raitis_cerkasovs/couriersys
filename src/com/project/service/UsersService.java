package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.dao.Authorities;
import com.project.dao.Client;
import com.project.dao.Courier;
import com.project.dao.CouriersDAO;
import com.project.dao.User;
import com.project.dao.UsersDAO;

/**
 * @author raitis
 * 
 * Wrapper class for DAO user, courier and client data update functions
 */
@Service("usersService")
public class UsersService {
	
	@Autowired
	private UsersDAO usersDao;
	
	@Autowired
	private CouriersDAO couriersDao;

	/**
	 * @param user - user object from form
	 * @param role - authority parameter
	 * 
	 * Function to save new user. Format user fields before saving
	 */
	public void saveUser(User user, String role) {
        	
        Authorities authorities = new Authorities();	
		authorities.setAuthority(role);
     	authorities.setUsername(user.getUsername());
     	
     	if (role.equals("COURIER")) {
     		user.setIscourier(true);
     	}
		
     	user.setAuthorities(authorities);
		user.setEnabled(true);
		
		usersDao.save(user);		
	}
	
	/**
	 * @param user - user object
	 * @param courier - courier object
	 * 
	 * Function to save new Courier. Format fields before saving
	 */
	public void saveCourier(User user, Courier courier) {
		
		courier.setUsername(user.getUsername());
		user.setCourier(courier);
		
		usersDao.saveOrUpdate(user);		
	}	

	
	/**
	 * @param user - user object
	 * @param client - courier object
	 * 
	 * Function to save new Client. Format fields before saving
	 */
	public void saveClient(User user, Client client) {
		
		client.setUsername(user.getUsername());
		user.setClient(client);
		
		usersDao.saveOrUpdate(user);		
	}
	
	
	public void saveOrUpdateUser(User user) {    	
		usersDao.saveOrUpdate(user);		
	}
	
	
	public void saveOrUpdateCourier(Courier courier) {
        couriersDao.saveOrUpdate(courier);		
	}
    
	
    public User getUser(String username) {
    	return usersDao.getUser(username);
    }

    
	public List<User> getUsers(String role) {
        return usersDao.getUsers(role);
	}	


	/**
	 * @return
	 * 
	 * Administrator function, not exploited in the project
	 */
	// @Secured("ROLE_ADMIN")
	public List<User> getAllUsers() {		
		return usersDao.getAllUsers();
	}

}
