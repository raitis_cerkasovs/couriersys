package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.dao.Client;
import com.project.dao.ClientsDAO;
import com.project.dao.Courier;
import com.project.dao.CouriersDAO;
import com.project.dao.Feedback;
import com.project.dao.FeedbacksDAO;
import com.project.dao.Job;
import com.project.dao.JobsDAO;


/**
 * @author raitis
 * 
 * Wrapper class for DAO Job, Courier, Client and Feedback class data access functions
 */
@Service("bookingsService")
public class BookingsService {
	
	@Autowired
	private JobsDAO jobsDao;	
	
	@Autowired
	private CouriersDAO couriersDao;	
	
	@Autowired
	private ClientsDAO clientsDao;

	@Autowired
	private FeedbacksDAO feedbacksDao;


	
	public void createOrUpdateJob(Job job) {
        jobsDao.saveOrUpdate(job);						
	}
	
	public void createOrUpdateFeedback(Feedback feedback) {
		feedbacksDao.saveOrUpdate(feedback);
	}

	public List<Job> getJobs(String couriersUsername) {
        return jobsDao.getJobs(couriersUsername);
	}

	public List<Feedback> getFeedbacks(String username) {
		return feedbacksDao.getFeedbacks(username);
	}
	
	public List<Job> getCourierJobs(String username) {
        return jobsDao.getCourierJobs(username);
	}
	

	public List<Job> getCourierJobsJson(String username) {
        return jobsDao.getCourierJobsJson(username);
	}

	public List<Courier> getAllWorkingCouriers() {
		return couriersDao.getAllWorkingCouriers();
	}

	public Courier getCourier(String username) {
		return couriersDao.getCourier(username);
	}

	public Client getClient(String username) {
        return clientsDao.getClient(username);
	}

	public Job getJob(Integer id) {
		return jobsDao.getJob(id);
	}
	
}
