
package com.project.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author raitis
 * 
 * Hibernate functions for Feedback class
 */
@Repository 
@Transactional
@Component("feedbacksDao")
public class FeedbacksDAO {


	@Autowired
	private SessionFactory sessionFactory;

	
	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@Transactional
	public void saveOrUpdate(Feedback feedback) {
	    session().saveOrUpdate(feedback);	
	    
	}


	@SuppressWarnings("unchecked")
	public List<Feedback> getFeedbacks(String username) {	
		
		Criteria crit = session().createCriteria(Feedback.class);
		
		crit.add(Restrictions.eq("courier", username));	

		return crit.list();		 
	}
}
