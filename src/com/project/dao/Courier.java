package com.project.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;


/**
 * @author raitis
 *
 * Class Courier, hold a row of courier table
 */
@Entity
@Table(name="couriers")
public class Courier implements Serializable {

	private static final long serialVersionUID = 3638057278096437350L;
	
	@Id
	@Column(name="username", unique=true, nullable=false)
	private String username;
	
	@OneToOne
    @PrimaryKeyJoinColumn
    @JsonIgnore
	private User user;		

	private String type;
	private boolean isworking;
	private boolean isavailable;
	private float lat;
	private float lon;	

	
	
	/**
	 * Constructors
	 */
	public Courier() {
	}

	public Courier(String username, User user, String type, boolean isworking,
			boolean isavailable, float lat, float lon) {
		this.username = username;
		this.user = user;
		this.type = type;
		this.isworking = isworking;
		this.isavailable = isavailable;
		this.lat = lat;
		this.lon = lon;
	}

	

	/**
	 * @return
	 * 
	 * Getters and Setters
	 */
	public String getUsername() {
		return username;
	}

	
	public void setUsername(String username) {
		this.username = username;
	}

	
	public User getUser() {
		return user;
	}
	

	public void setUser(User user) {
		this.user = user;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public boolean isIsworking() {
		return isworking;
	}


	public void setIsworking(boolean isworking) {
		this.isworking = isworking;
	}


	public boolean isIsavailable() {
		return isavailable;
	}


	public void setIsavailable(boolean isavailable) {
		this.isavailable = isavailable;
	}


	public float getLat() {
		return lat;
	}


	public void setLat(float lat) {
		this.lat = lat;
	}


	public float getLon() {
		return lon;
	}


	public void setLon(float lon) {
		this.lon = lon;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 * 
	 * Hash
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isavailable ? 1231 : 1237);
		result = prime * result + (isworking ? 1231 : 1237);
		result = prime * result + Float.floatToIntBits(lat);
		result = prime * result + Float.floatToIntBits(lon);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 * 
	 * Equals
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Courier other = (Courier) obj;
		if (isavailable != other.isavailable)
			return false;
		if (isworking != other.isworking)
			return false;
		if (Float.floatToIntBits(lat) != Float.floatToIntBits(other.lat))
			return false;
		if (Float.floatToIntBits(lon) != Float.floatToIntBits(other.lon))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * 
	 * toString
	 */
	@Override
	public String toString() {
		return username;
	}
		
}

