package com.project.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author raitis
 * 
 * Hibernate functions for Clients class
 */
@Repository 
@Transactional
@Component("clientsDao")
public class ClientsDAO {


	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@Transactional
	public void saveOrUpdate(Client client) {
	    session().saveOrUpdate(client);	
	    
	}

	public Client getClient(String username) {
		
		Criteria crit = session().createCriteria(Client.class);
		crit.add(Restrictions.idEq(username));
		
		return (Client)crit.uniqueResult();
		
	}
}
