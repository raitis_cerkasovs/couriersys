package com.project.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author raitis
 * 
 * Hibernate functions for Sendfrom class
 */
@Repository
@Transactional
@Component("sendfromDao")
public class SendfromDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	public List<Sendfrom> getSendfrom() {
		Criteria crit = session().createCriteria(Sendfrom.class);
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Sendfrom> getSendfrom(String username) {
		Criteria crit = session().createCriteria(Sendfrom.class);
		crit.add(Restrictions.eq("username", username));
		crit.add(Restrictions.eq("visible", true));   
		return crit.list();
	}

	public void saveOrUpdate(Sendfrom sendfrom) {
		session().saveOrUpdate(sendfrom);
	}

	//used only for potential maintenance purposes
	public boolean delete(int id) {
		Query query = session().createQuery("delete from Sendfrom where id=:id");
		query.setLong("id", id);
		return query.executeUpdate() == 1;
	}

	public Sendfrom getSendfrom(int id) {
		Criteria crit = session().createCriteria(Sendfrom.class);
		crit.add(Restrictions.idEq(id));
		return (Sendfrom)crit.uniqueResult();
	}

}
