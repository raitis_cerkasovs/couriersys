package com.project.dao;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;


@Entity
@Table(name="jobs")
public class Job implements Serializable {

	private static final long serialVersionUID = 3638057278096437350L;
	
	@Id
	@Column(name="id", unique=true, nullable=false)
	@GeneratedValue
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date created;
	
    @ManyToOne
    @JoinColumn(name = "sendto", nullable=false)
    @JsonIgnore
	private Sendto sendto; 	
	
    @ManyToOne
    @JoinColumn(name = "sendfrom", nullable=false)
    @JsonIgnore
	private Sendfrom sendfrom; 
    
    @ManyToOne
    @JoinColumn(name = "courier", nullable=false)
    @JsonIgnore
	private Courier courier;
    
    @ManyToOne
    @JoinColumn(name = "client", nullable=false)
    @JsonIgnore
	private Client client; 	  
		
	private String status;
	private String notes;
	private float distance;
	private float price;
	private boolean visible;
	private Time coltimefrom;
	private Time coltimetill;
	private Time deltimefrom;
	private Time deltimetill;
	private Date coldate;
	private Date deldate;
	
	// hand made sters, dont delete!!!
	public String getClientUsername() {
		return client.toString();
	}
	
	public String getCourierUsername() {
		return courier.toString();
	}
	//
	
	public Job() {
	}

	public Job(int id, java.util.Date created, Sendto sendto,
			Sendfrom sendfrom, Courier courier, Client client, String status,
			String notes, float distance, float price, boolean visible,
			Time coltimefrom, Time coltimetill, Time deltimefrom,
			Time deltimetill, Date coldate, Date deldate) {
		this.id = id;
		this.created = created;
		this.sendto = sendto;
		this.sendfrom = sendfrom;
		this.courier = courier;
		this.client = client;
		this.status = status;
		this.notes = notes;
		this.distance = distance;
		this.price = price;
		this.visible = visible;
		this.coltimefrom = coltimefrom;
		this.coltimetill = coltimetill;
		this.deltimefrom = deltimefrom;
		this.deltimetill = deltimetill;
		this.coldate = coldate;
		this.deldate = deldate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public java.util.Date getCreated() {
		return created;
	}

	public void setCreated(java.util.Date created) {
		this.created = created;
	}

	public Sendto getSendto() {
		return sendto;
	}

	public void setSendto(Sendto sendto) {
		this.sendto = sendto;
	}

	public Sendfrom getSendfrom() {
		return sendfrom;
	}

	public void setSendfrom(Sendfrom sendfrom) {
		this.sendfrom = sendfrom;
	}

	public Courier getCourier() {
		return courier;
	}

	public void setCourier(Courier courier) {
		this.courier = courier;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public Time getColtimefrom() {
		return coltimefrom;
	}

	public void setColtimefrom(Time coltimefrom) {
		this.coltimefrom = coltimefrom;
	}

	public Time getColtimetill() {
		return coltimetill;
	}

	public void setColtimetill(Time coltimetill) {
		this.coltimetill = coltimetill;
	}

	public Time getDeltimefrom() {
		return deltimefrom;
	}

	public void setDeltimefrom(Time deltimefrom) {
		this.deltimefrom = deltimefrom;
	}

	public Time getDeltimetill() {
		return deltimetill;
	}

	public void setDeltimetill(Time deltimetill) {
		this.deltimetill = deltimetill;
	}

	public Date getColdate() {
		return coldate;
	}

	public void setColdate(Date coldate) {
		this.coldate = coldate;
	}

	public Date getDeldate() {
		return deldate;
	}

	public void setDeldate(Date deldate) {
		this.deldate = deldate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + ((coldate == null) ? 0 : coldate.hashCode());
		result = prime * result
				+ ((coltimefrom == null) ? 0 : coltimefrom.hashCode());
		result = prime * result
				+ ((coltimetill == null) ? 0 : coltimetill.hashCode());
		result = prime * result + ((courier == null) ? 0 : courier.hashCode());
		result = prime * result + ((created == null) ? 0 : created.hashCode());
		result = prime * result + ((deldate == null) ? 0 : deldate.hashCode());
		result = prime * result
				+ ((deltimefrom == null) ? 0 : deltimefrom.hashCode());
		result = prime * result
				+ ((deltimetill == null) ? 0 : deltimetill.hashCode());
		result = prime * result + Float.floatToIntBits(distance);
		result = prime * result + id;
		result = prime * result + ((notes == null) ? 0 : notes.hashCode());
		result = prime * result + Float.floatToIntBits(price);
		result = prime * result
				+ ((sendfrom == null) ? 0 : sendfrom.hashCode());
		result = prime * result + ((sendto == null) ? 0 : sendto.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + (visible ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Job other = (Job) obj;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (coldate == null) {
			if (other.coldate != null)
				return false;
		} else if (!coldate.equals(other.coldate))
			return false;
		if (coltimefrom == null) {
			if (other.coltimefrom != null)
				return false;
		} else if (!coltimefrom.equals(other.coltimefrom))
			return false;
		if (coltimetill == null) {
			if (other.coltimetill != null)
				return false;
		} else if (!coltimetill.equals(other.coltimetill))
			return false;
		if (courier == null) {
			if (other.courier != null)
				return false;
		} else if (!courier.equals(other.courier))
			return false;
		if (created == null) {
			if (other.created != null)
				return false;
		} else if (!created.equals(other.created))
			return false;
		if (deldate == null) {
			if (other.deldate != null)
				return false;
		} else if (!deldate.equals(other.deldate))
			return false;
		if (deltimefrom == null) {
			if (other.deltimefrom != null)
				return false;
		} else if (!deltimefrom.equals(other.deltimefrom))
			return false;
		if (deltimetill == null) {
			if (other.deltimetill != null)
				return false;
		} else if (!deltimetill.equals(other.deltimetill))
			return false;
		if (Float.floatToIntBits(distance) != Float
				.floatToIntBits(other.distance))
			return false;
		if (id != other.id)
			return false;
		if (notes == null) {
			if (other.notes != null)
				return false;
		} else if (!notes.equals(other.notes))
			return false;
		if (Float.floatToIntBits(price) != Float.floatToIntBits(other.price))
			return false;
		if (sendfrom == null) {
			if (other.sendfrom != null)
				return false;
		} else if (!sendfrom.equals(other.sendfrom))
			return false;
		if (sendto == null) {
			if (other.sendto != null)
				return false;
		} else if (!sendto.equals(other.sendto))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (visible != other.visible)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Job [id=" + id + ", created=" + created + ", sendto=" + sendto
				+ ", sendfrom=" + sendfrom + ", courier=" + courier
				+ ", client=" + client + ", status=" + status + ", notes="
				+ notes + ", distance=" + distance + ", price=" + price
				+ ", visible=" + visible + ", coltimefrom=" + coltimefrom
				+ ", coltimetill=" + coltimetill + ", deltimefrom="
				+ deltimefrom + ", deltimetill=" + deltimetill + ", coldate="
				+ coldate + ", deldate=" + deldate + "]";
	}
	
	
}
