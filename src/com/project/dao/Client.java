package com.project.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


/**
 * @author raitis
 *
 * Class Client, hold a row of clients table
 */
@Entity
@Table(name="clients")
public class Client implements Serializable {

	private static final long serialVersionUID = 3638057278096437350L;
	
	@Id
	@Column(name="username", unique=true, nullable=false)
	private String username;
	
	@OneToOne
    @PrimaryKeyJoinColumn
	private User user;		

    private String text1;
    private String text2;
    
    
    /**
     * Constructors
     */
    public Client() {
    }

	public Client(String username, User user, String text1, String text2) {
		this.username = username;
		this.user = user;
		this.text1 = text1;
		this.text2 = text2;
	}

	
	/**
	 * Getters and Setters
	 */
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getText1() {
		return text1;
	}

	public void setText1(String text1) {
		this.text1 = text1;
	}

	public String getText2() {
		return text2;
	}

	public void setText2(String text2) {
		this.text2 = text2;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 * 
	 * Hash
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((text1 == null) ? 0 : text1.hashCode());
		result = prime * result + ((text2 == null) ? 0 : text2.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 * 
	 * Equals
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (text1 == null) {
			if (other.text1 != null)
				return false;
		} else if (!text1.equals(other.text1))
			return false;
		if (text2 == null) {
			if (other.text2 != null)
				return false;
		} else if (!text2.equals(other.text2))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * 
	 * toString
	 */
	@Override
	public String toString() {
		return username;
	}
    
    
}