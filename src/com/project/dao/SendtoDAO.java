package com.project.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author raitis
 * 
 * Hibernate functions for Sendto class
 */
@Repository
@Transactional
@Component("sendtoDao")
public class SendtoDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	public List<Sendto> getSendto() {
		Criteria crit = session().createCriteria(Sendto.class);
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Sendto> getSendto(String username) {
		Criteria crit = session().createCriteria(Sendto.class);
		crit.add(Restrictions.eq("username", username));   
		crit.add(Restrictions.eq("visible", true));   
		return crit.list();
	}

	public void saveOrUpdate(Sendto sendto) {
		session().saveOrUpdate(sendto);
	}

	//used only for potential maintenance purposes
	public boolean delete(int id) {
		Query query = session().createQuery("delete from Sendto where id=:id");
		query.setLong("id", id);
		return query.executeUpdate() == 1;
	}

	public Sendto getSendto(int id) {
		Criteria crit = session().createCriteria(Sendto.class);
		crit.add(Restrictions.idEq(id));
		return (Sendto)crit.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Sendto> getAllSendto() {	
		return session().createQuery("from Sendto").list();		 
	}

}
