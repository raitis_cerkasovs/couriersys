package com.project.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author raitis
 * 
 * Hibernate functions for Courier class
 */
@Repository 
@Transactional
@Component("couriersDao")
public class CouriersDAO {

	@SuppressWarnings("unused")
	private NamedParameterJdbcTemplate jdbc;

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	
	@Transactional
	public void saveOrUpdate(Courier courier) {
	    session().saveOrUpdate(courier);	
	    
	}

	public Courier getCourier(String username) {
		
		Criteria crit = session().createCriteria(Courier.class);
		crit.add(Restrictions.idEq(username));
		
		return (Courier)crit.uniqueResult();	
	}	
	
	// the search criteria is added during data population
	@SuppressWarnings("unchecked")
	public List<Courier> getAllWorkingCouriers() {	
		
		Criteria crit = session().createCriteria(Courier.class);
	//	crit.add(Restrictions.eq("isworking", true));		
		return crit.list();
	}
	
}
