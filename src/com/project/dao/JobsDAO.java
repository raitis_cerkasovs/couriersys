package com.project.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author raitis
 * 
 * Hibernate functions for Job class
 */
@Repository 
@Transactional
@Component("jobsDao")
public class JobsDAO {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	    
	@Transactional
		public void saveOrUpdate(Job job) {
		    session().saveOrUpdate(job);	    
	}

	public Job getJob(int id) {
		Criteria crit = session().createCriteria(Job.class);
		crit.add(Restrictions.idEq(id));
		return (Job)crit.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Job> getJobs(String username) {	
		
		Criteria crit = session().createCriteria(Job.class);
		
		crit.createAlias("client", "c");	
		crit.add(Restrictions.eq("c.username", username));	
		crit.add(Restrictions.eq("visible", true));	

		return crit.list();		 
	}
	
	@SuppressWarnings("unchecked")
	public List<Job> getCourierJobs(String username) {	
		
		Criteria crit = session().createCriteria(Job.class);
		
		crit.createAlias("courier", "c");	
		crit.add(Restrictions.eq("c.username", username));	

		return crit.list();		 
	}

	@SuppressWarnings("unchecked")
	public List<Job> getCourierJobsJson(String username) {
		
		Criteria crit = session().createCriteria(Job.class);
		
		crit.createAlias("courier", "c");	
		crit.add(Restrictions.eq("c.username", username));	
		
		crit.add(Restrictions.disjunction()
		    .add(Restrictions.eq("status", "Sent to Courier"))
		    .add(Restrictions.eq("status", "Approved"))
		    .add(Restrictions.eq("status", "Collected")));

		return crit.list();		
	}
	
}
