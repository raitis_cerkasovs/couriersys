package com.project.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;


/**
 * @author raitis
 *
 * Class Feedback, hold a row of feedbacks table
 */
@Entity
@Table(name = "feedbacks")
public class Feedback implements Serializable {

	private static final long serialVersionUID = -5679161448880463368L;

	@Id
	@Column(name="id", unique=true, nullable=false)
	@GeneratedValue
	private int id;

	private String courier;
	private String client;
	private Integer job;

	@Size(min = 2, max = 254)
	private String text;
	
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date created;

	private boolean visible;

	
	
	/**
	 * Constructors
	 */
	public Feedback() {
	}

	public Feedback(int id, String courier, String client, Integer job,
			String text, boolean visible) {
		this.id = id;
		this.courier = courier;
		this.client = client;
		this.job = job;
		this.text = text;
		this.visible = visible;
	}

	
	
	/**
	 * @return
	 * 
	 * Setters and Getters
	 */
	public java.util.Date getCreated() {
		return created;
	}

	public void setCreated(java.util.Date created) {
		this.created = created;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCourier() {
		return courier;
	}

	public void setCourier(String courier) {
		this.courier = courier;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public Integer getJob() {
		return job;
	}

	public void setJob(Integer job) {
		this.job = job;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 * 
	 * Hash
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + ((courier == null) ? 0 : courier.hashCode());
		result = prime * result + id;
		result = prime * result + ((job == null) ? 0 : job.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + (visible ? 1231 : 1237);
		return result;
	}

	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 * 
	 * Equals
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Feedback other = (Feedback) obj;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (courier == null) {
			if (other.courier != null)
				return false;
		} else if (!courier.equals(other.courier))
			return false;
		if (id != other.id)
			return false;
		if (job == null) {
			if (other.job != null)
				return false;
		} else if (!job.equals(other.job))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (visible != other.visible)
			return false;
		return true;
	}

	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * 
	 * toString
	 */
	@Override
	public String toString() {
		return "Feedback [id=" + id + ", courier=" + courier + ", client="
				+ client + ", job=" + job + ", text=" + text + ", visible="
				+ visible + "]";
	}
	
}